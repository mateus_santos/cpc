
-- -----------------------------------------------------
-- Schema clube_pontos
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `clube_pontos` DEFAULT CHARACTER SET utf8 ;
USE `clube_pontos` ;

-- -----------------------------------------------------
-- Table `clube_pontos`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`produto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(10) NOT NULL,
  `descricao` VARCHAR(150) NOT NULL,
  `pontos` INT NOT NULL,
  `imagem` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`pedido` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(15) NOT NULL,
  `usuario_id` INT NOT NULL,
  `total` INT NOT NULL,
  `data` DATETIME NOT NULL,
  `status` TINYINT(1) NOT NULL,
  `obs` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`carrinho`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`carrinho` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `pedidos_id` INT NOT NULL,
  `produto_id` INT NOT NULL,
  `quantidade` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Pedidos_has_produto_produto1_idx` (`produto_id` ASC),
  INDEX `fk_Pedidos_has_produto_Pedidos_idx` (`pedidos_id` ASC),
  CONSTRAINT `fk_Pedidos_has_produto_Pedidos`
    FOREIGN KEY (`pedidos_id`)
    REFERENCES `clube_pontos`.`pedido` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Pedidos_has_produto_produto1`
    FOREIGN KEY (`produto_id`)
    REFERENCES `clube_pontos`.`produto` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `clube_pontos`.`configuracao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clube_pontos`.`configuracao` (
  `id` INT NOT NULL,
  `desconto_maximo` DECIMAL(10,2) NOT NULL,
  `dt_inicial` DATE NOT NULL,
  `dt_final` DATE NOT NULL,
  `dias_bloqueio` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

